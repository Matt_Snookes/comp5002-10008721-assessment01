﻿using System;

namespace comp5002_10008721_assessment01
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            /*Declare variables
              var  "name"
              var  "answer"
              var  "number"     
              */
            var name = "0";
            var answer = "0";
            var number = 0.00;
            
            //"Welcome to Snookes' online computer supplies 
            Console.WriteLine("\n\tWelcome to Snookes' online computer supplies");
            //"Please enter your name"
            Console.WriteLine("\n\tPlease enter your name?");
            //Read entered name and store to variable "name"
            name = Console.ReadLine();
            //Hello "name" 
            Console.WriteLine($"\n\tHello {name}");
            //Please enter a number with 2 decimal places
            Console.WriteLine("\n\tPlease enter a number with 2 decimal places?");
            //Read entered number and save to "number"
            number = Double.Parse(Console.ReadLine());
            //You have entered "number""
            Console.WriteLine($"\n\tYou entered {number:c2}");
            //Would you like to enter another number? y / n
            Console.WriteLine("\n\tWould you like to add another number? y / n");
            //Read entered answer y or n and save to "answer"
            answer = Console.ReadLine();
            if (answer == "y" || answer == "Y")
            {
                //If user answered y Please enter another number?
                Console.WriteLine("\n\tPlease enter another number:");
                //Read entered number and add to variable "number"
                number += double.Parse(Console.ReadLine());
                //your total is "number"
                Console.WriteLine($"\n\tYour total is {number:c2}");
                //Press any key to continue to checkout
                Console.WriteLine("\n\tPress any key to continue to checkout");
                //Reads key press and continues to checkout
                Console.ReadKey();
            }
            else
            {
                //Your total is "number"
                Console.WriteLine($"\n\tYour total is {number:c2}");
                //Press any key to continue to checkout
                Console.WriteLine("\n\tPress any key to continue to checkout");
                //Reads key press and continues to checkout
                Console.ReadKey();
            }
            // calculate "number" * 1.15 and save to "number"
            number = (number*1.15);
            //Your total including GST is "number"
            Console.WriteLine($"\n\tYour total including GST is {number:c2}");
            //Thanks for shopping at Snookes' online computer supplies"
            Console.WriteLine("\n\tThanks for shopping at Snookes' online computer supplies");
            //Please come again
            Console.WriteLine("\n\tPlease come again");
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            //Press enter to quit the program
            Console.WriteLine("\tPress <Enter> to quit the program"); 
            //Quits program on key press
            Console.ReadKey();
        }
    }
}
